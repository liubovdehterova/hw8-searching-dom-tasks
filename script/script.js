/*
1.Опишіть своїми словами що таке Document Object Model (DOM)

Весь контент веб-сторінки стає структурою як дерево вузлів, де кожен елемент сторінки є вузлом
Весь контент стає доступним для програмного забезпечення, що дозволяє динамічно впливати на вміст та зовнішній вигляд сторінки
*/
/*
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
2.1 innerHTML:
        Він повертає або приймає рядок, який містить HTML - код.
        Це означає що ми можемо змінити структуру HTML - сторінки
2.2 innerText:
        Він повертає або приймає тільки текстовий зміст дочірніх елементів і не включає HTML-теги
*/

/*
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
За допомогою ідентифікатора (ID): document.getElementById();
За допомогою класу (Class): document.getElementsByClassName();
За допомогою тегу або імені елемента: document.getElementsByTagName();
За допомогою селекторів: document.querySelector(), document.querySelectorAll()
        document.querySelector() - повертає перший елемент, що відповідає селектору
        document.querySelectorAll() - повертає колекцію всіх елементів, що відповідають селектору.
document.getElementsByName() - використовуються для звернення до елементів за атрибутами
*/

/*
Знаходимо всі параграфи на сторінці та встановлюємо колір фону #ff0000
*/
const paragraph = document.querySelectorAll("p");
paragraph.forEach(elem => elem.style.background = "#ff0000");

/*
Знайти елемент із id="optionsList". Вивести у консоль.
Знайти батьківський елемент та вивести в консоль.
*/
const optionsList = document.getElementById('optionsList');
console.log('Знайти елемент із id="optionsList": ', optionsList);
console.log('батьківський елемент: ', optionsList.parentElement);

/*
Знайти дочірні ноди,якщо вони є, і вивести в консоль назви та тип нод.
*/
const childNodes = optionsList.childNodes;
console.log('дочірні ноди: ', childNodes);

childNodes.forEach((node) => {
        console.log('Назва: ', node.nodeName);
        console.log('Тип: ', node.nodeType);
});

/*
Встановіть в якості контента елемента з класом(елеммента з таким класом не було, але був id - використала id)
testParagraph наступний параграф -
This is a paragraph
*/
const elemTest = document.querySelector('#testParagraph ');
console.log(elemTest);
elemTest.textContent = 'This is a paragraph';

/*
Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль.
Кожному з елементів присвоїти новий клас nav-item.
*/
const mainHeaderElems = Array.from(document.querySelector('.main-header').children);
mainHeaderElems.forEach((elem) => {
        console.log(elem);
        elem.classList.add('nav-item');
});

/*Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.*/
const sectionTitle = document.querySelectorAll('.section-title');
sectionTitle.forEach(elem => elem.classList.remove('section-title'));
